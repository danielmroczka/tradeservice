package com.iov42.tradeplatform;

import org.junit.Test;

public class OrderTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptInvalidQuanity() {
        new Order(1, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptInvalidPrice() {
        new Order(0, 1);
    }
}
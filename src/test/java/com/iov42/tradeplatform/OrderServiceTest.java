package com.iov42.tradeplatform;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {
    private OrderService orderService;

    @Before
    public void setUp() {
        orderService = new OrderService();
    }

    @Test
    public void shouldCreateBuyOrder() {
        //GIVEN
        //WHEN
        orderService.buyOrder(new Order(100, 100));
        //THEN
        assertEquals(1, orderService.buyOrders.size());
        assertEquals(0, orderService.sellOrders.size());
    }

    @Test
    public void shouldCreateManyBuyOrders() {
        //GIVEN
        //WHEN
        orderService.buyOrder(new Order(100, 100));
        orderService.buyOrder(new Order(100, 100));
        orderService.buyOrder(new Order(100, 100));
        //THEN
        assertEquals(3, orderService.buyOrders.size());
        assertEquals(0, orderService.sellOrders.size());
    }

    @Test
    public void shouldCreateSellOrder() {
        //GIVEN
        //WHEN
        orderService.sellOrder(new Order(100, 100));
        //THEN
        assertEquals(0, orderService.buyOrders.size());
        assertEquals(1, orderService.sellOrders.size());
    }

    @Test
    public void shouldCreateManySellOrders() {
        //GIVEN
        //WHEN
        orderService.sellOrder(new Order(100, 100));
        orderService.sellOrder(new Order(100, 100));
        orderService.sellOrder(new Order(100, 100));
        //THEN
        assertEquals(0, orderService.buyOrders.size());
        assertEquals(3, orderService.sellOrders.size());
    }

    @Test
    public void shouldSellOrderSortedAscending() {
        //GIVEN
        //WHEN
        orderService.sellOrder(new Order(110, 100));
        orderService.sellOrder(new Order(105, 100));
        orderService.sellOrder(new Order(100, 100));
        //THEN
        List<Order> list = new ArrayList<>(orderService.sellOrders);
        assertEquals(100, list.get(0).getPrice());
        assertEquals(110, list.get(2).getPrice());
    }

    @Test
    public void shouldBuyOrderSortedDescending() {
        //GIVEN
        //WHEN
        orderService.buyOrder(new Order(105, 100));
        orderService.buyOrder(new Order(110, 100));
        orderService.buyOrder(new Order(100, 100));
        //THEN
        List<Order> list = new ArrayList<>(orderService.buyOrders);
        assertEquals(110, list.get(0).getPrice());
        assertEquals(100, list.get(2).getPrice());
    }

    @Test
    public void shouldCancelOrder() {
        //GIVEN
        Order order1 = orderService.buyOrder(new Order(100, 100));
        Order order2 = orderService.sellOrder(new Order(200, 100));
        //WHEN
        orderService.cancelOrder(order1);
        orderService.cancelOrder(order2);
        //THEN
        assertEquals(0, orderService.buyOrders.size());
        assertEquals(0, orderService.sellOrders.size());
        assertEquals(0, orderService.transactions.size());
    }

    @Test
    public void scenarioSimpleMatch() {
        //GIVEN
        orderService.buyOrder(new Order(100, 100));
        orderService.sellOrder(new Order(100, 100));
        //WHEN

        //THEN
        assertEquals(0, orderService.buyOrders.size());
        assertEquals(0, orderService.sellOrders.size());
        assertEquals(1, orderService.transactions.size());
        assertEquals(100, orderService.transactions.get(0).getPrice());
        assertEquals(100, orderService.transactions.get(0).getQuantity());
    }

    @Test
    public void scenarioSimpleOneNotMatched() {
        //GIVEN
        //WHEN
        orderService.buyOrder(new Order(100, 100));
        orderService.sellOrder(new Order(120, 100));
        //THEN
        assertEquals(1, orderService.buyOrders.size());
        assertEquals(1, orderService.sellOrders.size());
        assertEquals(0, orderService.transactions.size());
    }

    @Test
    public void scenarioEqualBids() {
        //GIVEN
        orderService.buyOrder(new Order(100, 1));
        orderService.buyOrder(new Order(105, 2));
        orderService.buyOrder(new Order(110, 3));
        orderService.sellOrder(new Order(110, 3));
        orderService.sellOrder(new Order(105, 2));
        orderService.sellOrder(new Order(100, 1));
        //WHEN

        //THEN
        assertEquals(0, orderService.buyOrders.size());
        assertEquals(0, orderService.sellOrders.size());

        assertEquals(3, orderService.transactions.size());
        assertEquals(110, orderService.transactions.get(0).getPrice());
        assertEquals(3, orderService.transactions.get(0).getQuantity());
        assertEquals(105, orderService.transactions.get(1).getPrice());
        assertEquals(2, orderService.transactions.get(1).getQuantity());
        assertEquals(100, orderService.transactions.get(2).getPrice());
        assertEquals(1, orderService.transactions.get(2).getQuantity());
    }

    @Test
    public void scenarioOneBuysManySell() {
        //GIVEN
        orderService.buyOrder(new Order(200, 600));
        orderService.sellOrder(new Order(100, 100));
        orderService.sellOrder(new Order(150, 200));
        orderService.sellOrder(new Order(200, 300));
        //WHEN

        //THEN
        assertEquals(0, orderService.buyOrders.size());
        assertEquals(0, orderService.sellOrders.size());
        assertEquals(3, orderService.transactions.size());
        assertEquals(100, orderService.transactions.get(0).getPrice());
        assertEquals(100, orderService.transactions.get(0).getQuantity());
        assertEquals(150, orderService.transactions.get(1).getPrice());
        assertEquals(200, orderService.transactions.get(1).getQuantity());
        assertEquals(200, orderService.transactions.get(2).getPrice());
        assertEquals(300, orderService.transactions.get(2).getQuantity());
    }

    @Test
    public void scenarioOneSellsManyBuy() {
        //GIVEN
        orderService.sellOrder(new Order(100, 600));
        orderService.buyOrder(new Order(200, 100));
        orderService.buyOrder(new Order(150, 200));
        orderService.buyOrder(new Order(100, 300));
        //WHEN

        //THEN
        assertEquals(0, orderService.sellOrders.size());
        assertEquals(0, orderService.buyOrders.size());
        assertEquals(3, orderService.transactions.size());

        assertEquals(100, orderService.transactions.get(0).getPrice());
        assertEquals(100, orderService.transactions.get(0).getQuantity());
        assertEquals(100, orderService.transactions.get(1).getPrice());
        assertEquals(200, orderService.transactions.get(1).getQuantity());
        assertEquals(100, orderService.transactions.get(2).getPrice());
        assertEquals(300, orderService.transactions.get(2).getQuantity());
    }

    @Test
    public void scenarioNothingShouldBeSold() {
        //GIVEN
        //WHEN
        orderService.sellOrder(new Order(200, 100));
        orderService.buyOrder(new Order(100, 100));
        orderService.buyOrder(new Order(100, 100));
        orderService.buyOrder(new Order(100, 100));
        //THEN
        assertEquals(1, orderService.sellOrders.size());
        assertEquals(3, orderService.buyOrders.size());
        assertEquals(0, orderService.transactions.size());
    }

    @Test
    public void scenarioBuyOrderLeft() {
        //GIVEN
        //WHEN
        orderService.sellOrder(new Order(100, 50));
        orderService.sellOrder(new Order(110, 60));
        orderService.buyOrder(new Order(120, 70));
        orderService.buyOrder(new Order(110, 100));

        //THEN
        assertEquals(0, orderService.sellOrders.size());
        assertEquals(1, orderService.buyOrders.size());

        assertEquals(3, orderService.transactions.size());
        assertEquals(100, orderService.transactions.get(0).getPrice());
        assertEquals(50, orderService.transactions.get(0).getQuantity());
        assertEquals(110, orderService.transactions.get(1).getPrice());
        assertEquals(20, orderService.transactions.get(1).getQuantity());
        assertEquals(110, orderService.transactions.get(2).getPrice());
        assertEquals(40, orderService.transactions.get(2).getQuantity());
    }

    @Test
    public void scenarioSellOrderLeft() {
        //GIVEN
        //WHEN
        orderService.buyOrder(new Order(100, 50));
        orderService.buyOrder(new Order(110, 60));
        orderService.sellOrder(new Order(120, 70));
        orderService.sellOrder(new Order(90, 110));

        //THEN
        assertEquals(1, orderService.sellOrders.size());
        assertEquals(0, orderService.buyOrders.size());

        assertEquals(2, orderService.transactions.size());
        assertEquals(90, orderService.transactions.get(0).getPrice());
        assertEquals(60, orderService.transactions.get(0).getQuantity());
        assertEquals(90, orderService.transactions.get(1).getPrice());
        assertEquals(50, orderService.transactions.get(1).getQuantity());
    }

    @Test
    public void expiredOrdersShouldNotBeConsider() {
        //GIVEN
        // WHEN
        orderService.buyOrder(new Order(100, 100, System.currentTimeMillis() - 1));
        orderService.sellOrder(new Order(100, 100, System.currentTimeMillis() - 1));
        //THEN
        assertEquals(0, orderService.buyOrders.size());
        assertEquals(0, orderService.sellOrders.size());
        assertEquals(0, orderService.transactions.size());

    }

}
package com.iov42.tradeplatform;

import java.util.*;

class OrderService {

    // Buy orders sorted descending by price and ascending by creation time
    private final Comparator<Order> buyOrderComparator = (o1, o2) -> (int) (o1.getPrice() != o2.getPrice() ? o2.getPrice() - o1.getPrice() : o1.getTimestamp() - o2.getTimestamp());

    // Sell orders sorted ascending by price and ascending by creation time
    private final Comparator<Order> sellOrderComparator = (o1, o2) -> (int) (o1.getPrice() != o2.getPrice() ? o1.getPrice() - o2.getPrice() : o1.getTimestamp() - o2.getTimestamp());

    final Set<Order> buyOrders = new TreeSet(buyOrderComparator);
    final Set<Order> sellOrders = new TreeSet(sellOrderComparator);
    final List<Transaction> transactions = new ArrayList();

    public Order buyOrder(Order order) {
        buyOrders.add(order);
        match();
        return order;
    }

    public Order sellOrder(Order order) {
        sellOrders.add(order);
        match();
        return order;
    }

    public void cancelOrder(Order order) {
        buyOrders.remove(order);
        sellOrders.remove(order);
    }

    public void printReport() {
        transactions.forEach(System.out::println);
    }

    private void match() {
        // Don't trigger matching logic if there is not at least one buy and sell order
        if (buyOrders.isEmpty() || sellOrders.isEmpty()) {
            return;
        }
        removeExpiredOrders(buyOrders);
        removeExpiredOrders(sellOrders);

        List<Order> ordersToClose = new ArrayList<>();
        Order[] buys = buyOrders.toArray(new Order[0]);
        Order[] sells = sellOrders.toArray(new Order[0]);

        for (int buyId = 0, sellId = 0; buyId < buys.length && sellId < sells.length; ) {
            Order buy = buys[buyId];
            Order sell = sells[sellId];

            /**
             * Sell offers are sorted ascending, buy offers are sorted descending.
             * If once sell price is higher than buy price it doesn't make sense to iterate further.
             */
            if (sell.getPrice() > buy.getPrice()) {
                break;
            }

            int quantity = Math.min(sell.getQuantity(), buy.getQuantity());
            int price = sell.getPrice();

            if (sell.getQuantity() > quantity) {
                // 1. Seller has more to sell in order
                sell.setQuantity(sell.getQuantity() - quantity);
            } else {
                // 2. Seller has sold everything, order must be removed and the next order should be taken
                ordersToClose.add(sell);
                sellId++;
            }

            if (buy.getQuantity() > quantity) {
                // 3. Buyer has more to buy in order
                buy.setQuantity(buy.getQuantity() - quantity);
            } else {
                // 4 Buyer has bought everything, order must be removed and the next order should be taken
                ordersToClose.add(buy);
                buyId++;
            }

            createTransaction(quantity, price);
        }

        ordersToClose.forEach(this::cancelOrder);
    }

    private void removeExpiredOrders(Set<Order> orders) {
        orders.removeIf(o -> o.getExpiration() < System.currentTimeMillis());
    }

    private void createTransaction(int availableQuantity, int price) {
        Transaction transaction = new Transaction(availableQuantity, price);
        transactions.add(transaction);
        System.out.println(transaction);
    }

}


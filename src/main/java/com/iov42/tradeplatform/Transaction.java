package com.iov42.tradeplatform;

/**
 * Class represents Transaction made between Seller and Buyer when the price matches
 */
class Transaction {
    private final int quantity;
    private final int price;

    public Transaction(int quantity, int price) {
        this.quantity = quantity;
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return String.format("Transaction{price=%d, quantity=%d}", price, quantity);
    }


}

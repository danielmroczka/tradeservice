package com.iov42.tradeplatform;

/**
 * Class represents Order made buy Seller or Buyer
 */
public class Order {

    private int quantity;
    /**
     * The bid price is what buyers are willing to pay for it.
     * The ask price is what sellers are willing to take for it.
     */
    private final int price;
    private final long timestamp;
    private final long expiration;

    public Order(int price, int quantity, long expiration) {
        if (quantity <= 0 || price <= 0) {
            throw new IllegalArgumentException("At least one of the argument is not correct!");
        }

        this.price = price;
        this.quantity = quantity;
        this.expiration = expiration;
        this.timestamp = System.nanoTime();
    }

    public Order(int price, int quantity) {
        this(price, quantity, Long.MAX_VALUE);
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public long getExpiration() {
        return expiration;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return String.format("Order{price=%d, quantity=%d}", price, quantity);
    }
}
